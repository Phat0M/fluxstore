import 'package:flutter/material.dart';
import 'package:progress_hud/progress_hud.dart';

@immutable
class ProgressDialog extends StatefulWidget {
  final Widget child;
  const ProgressDialog({
    @required this.child,
    Key key,
  })  : assert(child != null, 'Field child in widget Auth must not be null'),
        super(key: key);
  static _ProgressDialogState of(BuildContext context) =>
      _ProgressDialogScope.of(context)?.state;

  @override
  State<ProgressDialog> createState() => _ProgressDialogState();
}

class _ProgressDialogState extends State<ProgressDialog> {
  bool _isShow = false;

  void show() {
    setState(() {
      _isShow = true;
    });
  }

  void hide() {
    setState(() {
      _isShow = false;
    });
  }

  @override
  Widget build(BuildContext context) => _ProgressDialogScope(
        state: this,
        child: _isShow ? ProgressHUD() : widget.child,
      );
}

@immutable
class _ProgressDialogScope extends InheritedWidget {
  final _ProgressDialogState state;

  const _ProgressDialogScope({
    @required Widget child,
    @required this.state,
    Key key,
  })  : assert(
            child != null, 'Field child in widget _AuthScope must not be null'),
        assert(state is _ProgressDialogState, '_AuthState must not be null'),
        super(key: key, child: child);

  static _ProgressDialogScope of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<_ProgressDialogScope>();

  @override
  bool updateShouldNotify(_ProgressDialogScope oldWidget) =>
      !identical(state, oldWidget.state) || state != oldWidget.state;
}
