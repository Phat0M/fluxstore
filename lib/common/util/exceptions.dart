class UnauthorizedException implements Exception {
  @override
  String toString() {
    return 'UnauthorizedException(user is not authorized);';
  }
}
