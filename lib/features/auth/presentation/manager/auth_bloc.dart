import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluxstore/common/util/use_case.dart';
import 'package:fluxstore/features/auth/domain/use_cases/fetch_user_use_case.dart';
import 'package:fluxstore/features/auth/domain/use_cases/google_sign_in_use_case.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(AuthState initialState, this._googleSignIn, this._firebaseAuth,
      this._fetchUser)
      : super(initialState);

  final FetchUserUseCase _fetchUser;
  final GoogleSignInUseCase _googleSignIn;

  // Немного лень для этого расписывать, но по хорошему надо(в реальном проекте бы расписывал) юзкейсы для авторизация
  final FirebaseAuth _firebaseAuth;

  Stream<AuthState> _googleEvent(GoogleAuthEvent event) async* {
    yield AuthState.loading();
    final user = await _googleSignIn(NoParams());
    final storeUser = await _fetchUser(user.email);
    yield AuthState.authorized(storeUser);
  }

  Stream<AuthState> _signOutEvent(SignOutEvent event) async* {}

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    yield* event.when(
        google: () => _googleEvent(event), signOut: () => _signOutEvent(event));
  }
}
