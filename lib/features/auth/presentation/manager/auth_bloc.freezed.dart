// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AuthEventTearOff {
  const _$AuthEventTearOff();

// ignore: unused_element
  GoogleAuthEvent google() {
    return const GoogleAuthEvent();
  }

// ignore: unused_element
  SignOutEvent signOut() {
    return const SignOutEvent();
  }
}

/// @nodoc
// ignore: unused_element
const $AuthEvent = _$AuthEventTearOff();

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult google(),
    @required TResult signOut(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult google(),
    TResult signOut(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult google(GoogleAuthEvent value),
    @required TResult signOut(SignOutEvent value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult google(GoogleAuthEvent value),
    TResult signOut(SignOutEvent value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res> implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  final AuthEvent _value;
  // ignore: unused_field
  final $Res Function(AuthEvent) _then;
}

/// @nodoc
abstract class $GoogleAuthEventCopyWith<$Res> {
  factory $GoogleAuthEventCopyWith(
          GoogleAuthEvent value, $Res Function(GoogleAuthEvent) then) =
      _$GoogleAuthEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GoogleAuthEventCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements $GoogleAuthEventCopyWith<$Res> {
  _$GoogleAuthEventCopyWithImpl(
      GoogleAuthEvent _value, $Res Function(GoogleAuthEvent) _then)
      : super(_value, (v) => _then(v as GoogleAuthEvent));

  @override
  GoogleAuthEvent get _value => super._value as GoogleAuthEvent;
}

/// @nodoc
class _$GoogleAuthEvent implements GoogleAuthEvent {
  const _$GoogleAuthEvent();

  @override
  String toString() {
    return 'AuthEvent.google()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is GoogleAuthEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult google(),
    @required TResult signOut(),
  }) {
    assert(google != null);
    assert(signOut != null);
    return google();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult google(),
    TResult signOut(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (google != null) {
      return google();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult google(GoogleAuthEvent value),
    @required TResult signOut(SignOutEvent value),
  }) {
    assert(google != null);
    assert(signOut != null);
    return google(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult google(GoogleAuthEvent value),
    TResult signOut(SignOutEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (google != null) {
      return google(this);
    }
    return orElse();
  }
}

abstract class GoogleAuthEvent implements AuthEvent {
  const factory GoogleAuthEvent() = _$GoogleAuthEvent;
}

/// @nodoc
abstract class $SignOutEventCopyWith<$Res> {
  factory $SignOutEventCopyWith(
          SignOutEvent value, $Res Function(SignOutEvent) then) =
      _$SignOutEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignOutEventCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements $SignOutEventCopyWith<$Res> {
  _$SignOutEventCopyWithImpl(
      SignOutEvent _value, $Res Function(SignOutEvent) _then)
      : super(_value, (v) => _then(v as SignOutEvent));

  @override
  SignOutEvent get _value => super._value as SignOutEvent;
}

/// @nodoc
class _$SignOutEvent implements SignOutEvent {
  const _$SignOutEvent();

  @override
  String toString() {
    return 'AuthEvent.signOut()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SignOutEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult google(),
    @required TResult signOut(),
  }) {
    assert(google != null);
    assert(signOut != null);
    return signOut();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult google(),
    TResult signOut(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signOut != null) {
      return signOut();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult google(GoogleAuthEvent value),
    @required TResult signOut(SignOutEvent value),
  }) {
    assert(google != null);
    assert(signOut != null);
    return signOut(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult google(GoogleAuthEvent value),
    TResult signOut(SignOutEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signOut != null) {
      return signOut(this);
    }
    return orElse();
  }
}

abstract class SignOutEvent implements AuthEvent {
  const factory SignOutEvent() = _$SignOutEvent;
}

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

// ignore: unused_element
  AuthorizedState authorized(StoreUser user) {
    return AuthorizedState(
      user,
    );
  }

// ignore: unused_element
  UnauthorizedState unauthorized() {
    return const UnauthorizedState();
  }

// ignore: unused_element
  LoadingState loading() {
    return const LoadingState();
  }
}

/// @nodoc
// ignore: unused_element
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authorized(StoreUser user),
    @required TResult unauthorized(),
    @required TResult loading(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authorized(StoreUser user),
    TResult unauthorized(),
    TResult loading(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authorized(AuthorizedState value),
    @required TResult unauthorized(UnauthorizedState value),
    @required TResult loading(LoadingState value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authorized(AuthorizedState value),
    TResult unauthorized(UnauthorizedState value),
    TResult loading(LoadingState value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;
}

/// @nodoc
abstract class $AuthorizedStateCopyWith<$Res> {
  factory $AuthorizedStateCopyWith(
          AuthorizedState value, $Res Function(AuthorizedState) then) =
      _$AuthorizedStateCopyWithImpl<$Res>;
  $Res call({StoreUser user});
}

/// @nodoc
class _$AuthorizedStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements $AuthorizedStateCopyWith<$Res> {
  _$AuthorizedStateCopyWithImpl(
      AuthorizedState _value, $Res Function(AuthorizedState) _then)
      : super(_value, (v) => _then(v as AuthorizedState));

  @override
  AuthorizedState get _value => super._value as AuthorizedState;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(AuthorizedState(
      user == freezed ? _value.user : user as StoreUser,
    ));
  }
}

/// @nodoc
class _$AuthorizedState implements AuthorizedState {
  const _$AuthorizedState(this.user) : assert(user != null);

  @override
  final StoreUser user;

  @override
  String toString() {
    return 'AuthState.authorized(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AuthorizedState &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @override
  $AuthorizedStateCopyWith<AuthorizedState> get copyWith =>
      _$AuthorizedStateCopyWithImpl<AuthorizedState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authorized(StoreUser user),
    @required TResult unauthorized(),
    @required TResult loading(),
  }) {
    assert(authorized != null);
    assert(unauthorized != null);
    assert(loading != null);
    return authorized(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authorized(StoreUser user),
    TResult unauthorized(),
    TResult loading(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authorized != null) {
      return authorized(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authorized(AuthorizedState value),
    @required TResult unauthorized(UnauthorizedState value),
    @required TResult loading(LoadingState value),
  }) {
    assert(authorized != null);
    assert(unauthorized != null);
    assert(loading != null);
    return authorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authorized(AuthorizedState value),
    TResult unauthorized(UnauthorizedState value),
    TResult loading(LoadingState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (authorized != null) {
      return authorized(this);
    }
    return orElse();
  }
}

abstract class AuthorizedState implements AuthState {
  const factory AuthorizedState(StoreUser user) = _$AuthorizedState;

  StoreUser get user;
  $AuthorizedStateCopyWith<AuthorizedState> get copyWith;
}

/// @nodoc
abstract class $UnauthorizedStateCopyWith<$Res> {
  factory $UnauthorizedStateCopyWith(
          UnauthorizedState value, $Res Function(UnauthorizedState) then) =
      _$UnauthorizedStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnauthorizedStateCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res>
    implements $UnauthorizedStateCopyWith<$Res> {
  _$UnauthorizedStateCopyWithImpl(
      UnauthorizedState _value, $Res Function(UnauthorizedState) _then)
      : super(_value, (v) => _then(v as UnauthorizedState));

  @override
  UnauthorizedState get _value => super._value as UnauthorizedState;
}

/// @nodoc
class _$UnauthorizedState implements UnauthorizedState {
  const _$UnauthorizedState();

  @override
  String toString() {
    return 'AuthState.unauthorized()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnauthorizedState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authorized(StoreUser user),
    @required TResult unauthorized(),
    @required TResult loading(),
  }) {
    assert(authorized != null);
    assert(unauthorized != null);
    assert(loading != null);
    return unauthorized();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authorized(StoreUser user),
    TResult unauthorized(),
    TResult loading(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unauthorized != null) {
      return unauthorized();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authorized(AuthorizedState value),
    @required TResult unauthorized(UnauthorizedState value),
    @required TResult loading(LoadingState value),
  }) {
    assert(authorized != null);
    assert(unauthorized != null);
    assert(loading != null);
    return unauthorized(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authorized(AuthorizedState value),
    TResult unauthorized(UnauthorizedState value),
    TResult loading(LoadingState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unauthorized != null) {
      return unauthorized(this);
    }
    return orElse();
  }
}

abstract class UnauthorizedState implements AuthState {
  const factory UnauthorizedState() = _$UnauthorizedState;
}

/// @nodoc
abstract class $LoadingStateCopyWith<$Res> {
  factory $LoadingStateCopyWith(
          LoadingState value, $Res Function(LoadingState) then) =
      _$LoadingStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadingStateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements $LoadingStateCopyWith<$Res> {
  _$LoadingStateCopyWithImpl(
      LoadingState _value, $Res Function(LoadingState) _then)
      : super(_value, (v) => _then(v as LoadingState));

  @override
  LoadingState get _value => super._value as LoadingState;
}

/// @nodoc
class _$LoadingState implements LoadingState {
  const _$LoadingState();

  @override
  String toString() {
    return 'AuthState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadingState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult authorized(StoreUser user),
    @required TResult unauthorized(),
    @required TResult loading(),
  }) {
    assert(authorized != null);
    assert(unauthorized != null);
    assert(loading != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult authorized(StoreUser user),
    TResult unauthorized(),
    TResult loading(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult authorized(AuthorizedState value),
    @required TResult unauthorized(UnauthorizedState value),
    @required TResult loading(LoadingState value),
  }) {
    assert(authorized != null);
    assert(unauthorized != null);
    assert(loading != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult authorized(AuthorizedState value),
    TResult unauthorized(UnauthorizedState value),
    TResult loading(LoadingState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingState implements AuthState {
  const factory LoadingState() = _$LoadingState;
}
