part of 'auth_bloc.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState.authorized(StoreUser user) = AuthorizedState;
  const factory AuthState.unauthorized() = UnauthorizedState;
  const factory AuthState.loading() = LoadingState;
}
