import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluxstore/common/util/widgets/progress_dialog.dart';
import 'package:fluxstore/features/auth/presentation/manager/auth_bloc.dart';
import 'package:provider/provider.dart';

// В этот виджет оборачиваем всё, где нужна авторизация

@immutable
class AuthScreen extends StatefulWidget {
  final Widget child;
  const AuthScreen({
    @required this.child,
    Key key,
  })  : assert(child != null, 'Field child in widget Auth must not be null'),
        super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) => BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) {
          state.maybeWhen(
            loading: () {
              ProgressDialog.of(context)?.show();
            },
            orElse: () {
              ProgressDialog.of(context)?.hide();
            },
          );
        },
        buildWhen: (old, now) {
          return now is AuthorizedState || now is UnauthorizedState;
        },
        builder: (context, state) => _AuthScope(
          state: this,
          child: state.maybeWhen(
            authorized: (user) => widget.child,
            orElse: () => _AuthScreen(),
          ),
        ),
      );
}

@immutable
class _AuthScope extends InheritedWidget {
  final _AuthScreenState state;

  const _AuthScope({
    @required Widget child,
    @required this.state,
    Key key,
  })  : assert(
            child != null, 'Field child in widget _AuthScope must not be null'),
        assert(state is _AuthScreenState, '_AuthState must not be null'),
        super(key: key, child: child);

  /// Find _AuthScope in BuildContext
  static _AuthScope of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<_AuthScope>();

  @override
  bool updateShouldNotify(_AuthScope oldWidget) =>
      !identical(state, oldWidget.state) || state != oldWidget.state;
}

class _AuthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: InkWell(
          child: Text(
            'SignIn',
            style: TextStyle(
              color: Color(0xff5ece7b),
              fontSize: 22,
              fontWeight: FontWeight.w300,
            ),
          ),
          onTap: () {
            context.read<AuthBloc>().add(AuthEvent.google());
          },
        ),
      ),
    );
  }
}
