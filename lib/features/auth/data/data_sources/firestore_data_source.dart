import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';

class FirestoreDataSource {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<List<StoreUser>> fetchUsers() async {
    CollectionReference reference = firestore.collection('users');
    QuerySnapshot list = await reference.get();
    return list.docs.map((e) => StoreUser.fromJson(e.data())).toList();
  }

  Future<void> createUser(StoreUser user) async {
    CollectionReference _reference = firestore.collection('users');
    final snapshot = await _reference.doc(user.id).get();
  }
}
