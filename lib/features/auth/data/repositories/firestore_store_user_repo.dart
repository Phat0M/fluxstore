import 'package:fluxstore/features/auth/data/data_sources/firestore_data_source.dart';
import 'package:fluxstore/features/auth/domain/repositories/store_user_repo.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';

class FirestoreStoreUserRepo extends StoreUserRepo {
  final FirestoreDataSource _datasource;

  FirestoreStoreUserRepo(this._datasource);

  @override
  Future<void> createUser(StoreUser user) {
    _datasource.createUser(user);
  }

  @override
  Future<StoreUser> fetchUser(String email) async {
    final users = (await _datasource.fetchUsers())
        .where((element) => element.email == email)
        .toList();
    return users.length > 0 ? users.first : null;
  }
}
