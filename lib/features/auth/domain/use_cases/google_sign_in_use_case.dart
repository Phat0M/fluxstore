import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluxstore/common/util/use_case.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleSignInUseCase extends UseCase<User, NoParams> {
  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;

  GoogleSignInUseCase(this._firebaseAuth, this._googleSignIn);

  @override
  Future<User> call(NoParams _) async {
    final googleAcc = await _googleSignIn.signIn();
    final googleAuth = await googleAcc.authentication;
    await _firebaseAuth.signInWithCredential(GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken, idToken: googleAuth.idToken));
    return _firebaseAuth.currentUser;
  }
}
