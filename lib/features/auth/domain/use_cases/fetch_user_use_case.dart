import 'package:fluxstore/common/util/use_case.dart';
import 'package:fluxstore/features/auth/domain/repositories/store_user_repo.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:uuid/uuid.dart';

class FetchUserUseCase extends UseCase<StoreUser, String> {
  final StoreUserRepo _repo;

  FetchUserUseCase(this._repo);

  Future<StoreUser> _buildUser(String email) async {
    final user = StoreUser(favorites: [], id: Uuid().v1(), email: email);
    await _repo.createUser(user);
    return user;
  }

  @override
  Future<StoreUser> call(String params) async {
    return (await _repo.fetchUser(params)) ?? (await _buildUser(params));
  }
}
