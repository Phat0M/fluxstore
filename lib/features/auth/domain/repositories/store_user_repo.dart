import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';

abstract class StoreUserRepo {
  Future<StoreUser> fetchUser(String email);

  Future<void> createUser(StoreUser user);
}
