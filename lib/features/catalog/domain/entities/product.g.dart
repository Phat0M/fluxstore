// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as String,
    name: json['name'] as String,
    description: json['description'] as String,
    productCode: json['productCode'] as String,
    material: json['material'] as String,
    country: json['country'] as String,
    price: (json['price'] as num)?.toDouble(),
    discountPrice: (json['discountPrice'] as num)?.toDouble(),
    rating: json['rating'] as int,
    ratingNumber: json['ratingNumber'] as int,
    imageUrl: json['imageUrl'] as String,
    isFavorite: json['isFavorite'] as bool,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'productCode': instance.productCode,
      'material': instance.material,
      'country': instance.country,
      'price': instance.price,
      'discountPrice': instance.discountPrice,
      'rating': instance.rating,
      'ratingNumber': instance.ratingNumber,
      'imageUrl': instance.imageUrl,
      'isFavorite': instance.isFavorite,
    };
