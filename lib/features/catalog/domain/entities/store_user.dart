import 'package:freezed_annotation/freezed_annotation.dart';

part 'store_user.g.dart';

@JsonSerializable()
class StoreUser {
  final String email;
  final String id;
  final List<String> favorites;

  const StoreUser({
    @required this.email,
    @required this.id,
    @required this.favorites,
  });

  factory StoreUser.fromJson(Map<String, dynamic> json) =>
      _$StoreUserFromJson(json);

  Map<String, dynamic> toJson() => _$StoreUserToJson(this);
  @override
  String toString() {
    // TODO: implement toString
    return 'StoreUser($favorites)';
  }
}
