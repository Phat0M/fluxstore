// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StoreUser _$StoreUserFromJson(Map<String, dynamic> json) {
  return StoreUser(
    email: json['email'] as String,
    id: json['id'] as String,
    favorites: (json['favorites'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$StoreUserToJson(StoreUser instance) => <String, dynamic>{
      'email': instance.email,
      'id': instance.id,
      'favorites': instance.favorites,
    };
