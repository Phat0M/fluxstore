import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'product.g.dart';

@JsonSerializable()
class Product {
  final String id;
  final String name;
  final String description;
  final String productCode;
  final String material;
  final String country;
  final double price;
  final double discountPrice;
  final int rating;
  final int ratingNumber;
  final String imageUrl;
  final bool isFavorite;

  const Product({
    @required this.id,
    @required this.name,
    @required this.description,
    @required this.productCode,
    @required this.material,
    @required this.country,
    @required this.price,
    this.discountPrice,
    @required this.rating,
    @required this.ratingNumber,
    @required this.imageUrl,
    this.isFavorite,
  });
  
  Product copyWith({
    String id,
    String name,
    String description,
    String productCode,
    String material,
    String country,
    double price,
    String discountPrice,
    int rating,
    int ratingNumber,
    String imageUrl,
    bool isFavorite,
  }) {
    return Product(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      productCode: productCode ?? this.productCode,
      material: material ?? this.material,
      country: country ?? this.country,
      price: price ?? this.price,
      discountPrice: discountPrice ?? this.discountPrice,
      rating: rating ?? this.rating,
      ratingNumber: ratingNumber ?? this.ratingNumber,
      imageUrl: imageUrl ?? this.imageUrl,
      isFavorite: isFavorite ?? this.isFavorite,
    );
  }

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);

  @override
  String toString() {
    return 'Product(key: $id, fav: $isFavorite)';
  }
}
