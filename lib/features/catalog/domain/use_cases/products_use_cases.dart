import 'package:fluxstore/common/util/exceptions.dart';
import 'package:fluxstore/common/util/use_case.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:fluxstore/features/catalog/domain/repositories/products_repo.dart';
import 'package:fluxstore/features/catalog/domain/repositories/store_user_repo.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'products_use_cases.freezed.dart';

@freezed
abstract class ProductsUseCasesParam with _$ProductsUseCasesParam {
  const factory ProductsUseCasesParam.all({
    @Default(0) int page,
  }) = AllParams;
  const factory ProductsUseCasesParam.search(
    String search, {
    @Default(0) int page,
  }) = SearchParams;

  const factory ProductsUseCasesParam.makeFavorite(String key) = FavoriteParams;
}

class FetchAllUseCase extends UseCase<List<Product>, AllParams> {
  final ProductsRepo _repo;
  final StoreUser _user;

  FetchAllUseCase(this._repo, this._user);

  @override
  Future<List<Product>> call(AllParams params) async {
    var list = await _repo?.fetchAll(page: params.page);
    if (_user != null) {
      list = list
          .map((e) => e.copyWith(isFavorite: _user.favorites.contains(e.id)))
          .toList();
    }
    return list;
  }
}

class FetchSearchUseCase extends UseCase<List<Product>, SearchParams> {
  final ProductsRepo _repo;
  final StoreUser _user;

  FetchSearchUseCase(this._repo, this._user);

  @override
  Future<List<Product>> call(SearchParams params) async {
    var list = await _repo?.fetchSearch(params.search, page: params.page);
    if (_user != null) {
      list = list
          .map((e) =>
              e.copyWith(isFavorite: _user.favorites?.contains(e.id) ?? false))
          .toList()
          .where((element) => element.name.contains(params.search))
          .toList();
    }
    return list;
  }
}

class MakeFavoriteUseCase extends UseCase<void, FavoriteParams> {
  final StoreUserRepo _repo;
  final StoreUser _currentUser;

  MakeFavoriteUseCase(this._repo, this._currentUser);

  @override
  Future<void> call(FavoriteParams params) {
    _currentUser.favorites.add(params.key);
    if (_currentUser == null) {
      throw UnauthorizedException();
    }
    return _repo.updateUser(_currentUser);
  }
}
