// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'products_use_cases.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$ProductsUseCasesParamTearOff {
  const _$ProductsUseCasesParamTearOff();

// ignore: unused_element
  AllParams all({int page = 0}) {
    return AllParams(
      page: page,
    );
  }

// ignore: unused_element
  SearchParams search(String search, {int page = 0}) {
    return SearchParams(
      search,
      page: page,
    );
  }

// ignore: unused_element
  FavoriteParams makeFavorite(String key) {
    return FavoriteParams(
      key,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $ProductsUseCasesParam = _$ProductsUseCasesParamTearOff();

/// @nodoc
mixin _$ProductsUseCasesParam {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult all(int page),
    @required TResult search(String search, int page),
    @required TResult makeFavorite(String key),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult all(int page),
    TResult search(String search, int page),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult all(AllParams value),
    @required TResult search(SearchParams value),
    @required TResult makeFavorite(FavoriteParams value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult all(AllParams value),
    TResult search(SearchParams value),
    TResult makeFavorite(FavoriteParams value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $ProductsUseCasesParamCopyWith<$Res> {
  factory $ProductsUseCasesParamCopyWith(ProductsUseCasesParam value,
          $Res Function(ProductsUseCasesParam) then) =
      _$ProductsUseCasesParamCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProductsUseCasesParamCopyWithImpl<$Res>
    implements $ProductsUseCasesParamCopyWith<$Res> {
  _$ProductsUseCasesParamCopyWithImpl(this._value, this._then);

  final ProductsUseCasesParam _value;
  // ignore: unused_field
  final $Res Function(ProductsUseCasesParam) _then;
}

/// @nodoc
abstract class $AllParamsCopyWith<$Res> {
  factory $AllParamsCopyWith(AllParams value, $Res Function(AllParams) then) =
      _$AllParamsCopyWithImpl<$Res>;
  $Res call({int page});
}

/// @nodoc
class _$AllParamsCopyWithImpl<$Res>
    extends _$ProductsUseCasesParamCopyWithImpl<$Res>
    implements $AllParamsCopyWith<$Res> {
  _$AllParamsCopyWithImpl(AllParams _value, $Res Function(AllParams) _then)
      : super(_value, (v) => _then(v as AllParams));

  @override
  AllParams get _value => super._value as AllParams;

  @override
  $Res call({
    Object page = freezed,
  }) {
    return _then(AllParams(
      page: page == freezed ? _value.page : page as int,
    ));
  }
}

/// @nodoc
class _$AllParams implements AllParams {
  const _$AllParams({this.page = 0}) : assert(page != null);

  @JsonKey(defaultValue: 0)
  @override
  final int page;

  @override
  String toString() {
    return 'ProductsUseCasesParam.all(page: $page)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is AllParams &&
            (identical(other.page, page) ||
                const DeepCollectionEquality().equals(other.page, page)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(page);

  @override
  $AllParamsCopyWith<AllParams> get copyWith =>
      _$AllParamsCopyWithImpl<AllParams>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult all(int page),
    @required TResult search(String search, int page),
    @required TResult makeFavorite(String key),
  }) {
    assert(all != null);
    assert(search != null);
    assert(makeFavorite != null);
    return all(page);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult all(int page),
    TResult search(String search, int page),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (all != null) {
      return all(page);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult all(AllParams value),
    @required TResult search(SearchParams value),
    @required TResult makeFavorite(FavoriteParams value),
  }) {
    assert(all != null);
    assert(search != null);
    assert(makeFavorite != null);
    return all(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult all(AllParams value),
    TResult search(SearchParams value),
    TResult makeFavorite(FavoriteParams value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (all != null) {
      return all(this);
    }
    return orElse();
  }
}

abstract class AllParams implements ProductsUseCasesParam {
  const factory AllParams({int page}) = _$AllParams;

  int get page;
  $AllParamsCopyWith<AllParams> get copyWith;
}

/// @nodoc
abstract class $SearchParamsCopyWith<$Res> {
  factory $SearchParamsCopyWith(
          SearchParams value, $Res Function(SearchParams) then) =
      _$SearchParamsCopyWithImpl<$Res>;
  $Res call({String search, int page});
}

/// @nodoc
class _$SearchParamsCopyWithImpl<$Res>
    extends _$ProductsUseCasesParamCopyWithImpl<$Res>
    implements $SearchParamsCopyWith<$Res> {
  _$SearchParamsCopyWithImpl(
      SearchParams _value, $Res Function(SearchParams) _then)
      : super(_value, (v) => _then(v as SearchParams));

  @override
  SearchParams get _value => super._value as SearchParams;

  @override
  $Res call({
    Object search = freezed,
    Object page = freezed,
  }) {
    return _then(SearchParams(
      search == freezed ? _value.search : search as String,
      page: page == freezed ? _value.page : page as int,
    ));
  }
}

/// @nodoc
class _$SearchParams implements SearchParams {
  const _$SearchParams(this.search, {this.page = 0})
      : assert(search != null),
        assert(page != null);

  @override
  final String search;
  @JsonKey(defaultValue: 0)
  @override
  final int page;

  @override
  String toString() {
    return 'ProductsUseCasesParam.search(search: $search, page: $page)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SearchParams &&
            (identical(other.search, search) ||
                const DeepCollectionEquality().equals(other.search, search)) &&
            (identical(other.page, page) ||
                const DeepCollectionEquality().equals(other.page, page)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(search) ^
      const DeepCollectionEquality().hash(page);

  @override
  $SearchParamsCopyWith<SearchParams> get copyWith =>
      _$SearchParamsCopyWithImpl<SearchParams>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult all(int page),
    @required TResult search(String search, int page),
    @required TResult makeFavorite(String key),
  }) {
    assert(all != null);
    assert(search != null);
    assert(makeFavorite != null);
    return search(this.search, page);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult all(int page),
    TResult search(String search, int page),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (search != null) {
      return search(this.search, page);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult all(AllParams value),
    @required TResult search(SearchParams value),
    @required TResult makeFavorite(FavoriteParams value),
  }) {
    assert(all != null);
    assert(search != null);
    assert(makeFavorite != null);
    return search(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult all(AllParams value),
    TResult search(SearchParams value),
    TResult makeFavorite(FavoriteParams value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (search != null) {
      return search(this);
    }
    return orElse();
  }
}

abstract class SearchParams implements ProductsUseCasesParam {
  const factory SearchParams(String search, {int page}) = _$SearchParams;

  String get search;
  int get page;
  $SearchParamsCopyWith<SearchParams> get copyWith;
}

/// @nodoc
abstract class $FavoriteParamsCopyWith<$Res> {
  factory $FavoriteParamsCopyWith(
          FavoriteParams value, $Res Function(FavoriteParams) then) =
      _$FavoriteParamsCopyWithImpl<$Res>;
  $Res call({String key});
}

/// @nodoc
class _$FavoriteParamsCopyWithImpl<$Res>
    extends _$ProductsUseCasesParamCopyWithImpl<$Res>
    implements $FavoriteParamsCopyWith<$Res> {
  _$FavoriteParamsCopyWithImpl(
      FavoriteParams _value, $Res Function(FavoriteParams) _then)
      : super(_value, (v) => _then(v as FavoriteParams));

  @override
  FavoriteParams get _value => super._value as FavoriteParams;

  @override
  $Res call({
    Object key = freezed,
  }) {
    return _then(FavoriteParams(
      key == freezed ? _value.key : key as String,
    ));
  }
}

/// @nodoc
class _$FavoriteParams implements FavoriteParams {
  const _$FavoriteParams(this.key) : assert(key != null);

  @override
  final String key;

  @override
  String toString() {
    return 'ProductsUseCasesParam.makeFavorite(key: $key)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is FavoriteParams &&
            (identical(other.key, key) ||
                const DeepCollectionEquality().equals(other.key, key)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(key);

  @override
  $FavoriteParamsCopyWith<FavoriteParams> get copyWith =>
      _$FavoriteParamsCopyWithImpl<FavoriteParams>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult all(int page),
    @required TResult search(String search, int page),
    @required TResult makeFavorite(String key),
  }) {
    assert(all != null);
    assert(search != null);
    assert(makeFavorite != null);
    return makeFavorite(key);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult all(int page),
    TResult search(String search, int page),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (makeFavorite != null) {
      return makeFavorite(key);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult all(AllParams value),
    @required TResult search(SearchParams value),
    @required TResult makeFavorite(FavoriteParams value),
  }) {
    assert(all != null);
    assert(search != null);
    assert(makeFavorite != null);
    return makeFavorite(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult all(AllParams value),
    TResult search(SearchParams value),
    TResult makeFavorite(FavoriteParams value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (makeFavorite != null) {
      return makeFavorite(this);
    }
    return orElse();
  }
}

abstract class FavoriteParams implements ProductsUseCasesParam {
  const factory FavoriteParams(String key) = _$FavoriteParams;

  String get key;
  $FavoriteParamsCopyWith<FavoriteParams> get copyWith;
}
