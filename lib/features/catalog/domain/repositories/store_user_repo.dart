import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';

abstract class StoreUserRepo {
  Future<void> updateUser(StoreUser user);
}
