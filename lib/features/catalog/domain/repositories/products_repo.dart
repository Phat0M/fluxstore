import 'package:fluxstore/features/catalog/domain/entities/product.dart';

abstract class ProductsRepo {
  Future<List<Product>> fetchAll({int page = 0});

  Future<List<Product>> fetchSearch(String search, {int page = 0});
}
