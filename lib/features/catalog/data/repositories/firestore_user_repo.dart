import 'package:fluxstore/features/catalog/data/data_sources/firestore_data_source.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:fluxstore/features/catalog/domain/repositories/store_user_repo.dart';

class FirestoreUserRepo extends StoreUserRepo {
  final FirestoreDataSource _datasource;

  FirestoreUserRepo(this._datasource);

  @override
  Future<void> updateUser(StoreUser user) {
    return _datasource.updateUser(user);
  }
}
