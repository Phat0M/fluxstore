import 'package:fluxstore/features/catalog/data/data_sources/firestore_data_source.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';
import 'package:fluxstore/features/catalog/domain/repositories/products_repo.dart';

class FirestoreProductsRepo extends ProductsRepo {
  final FirestoreDataSource _datasource;

  FirestoreProductsRepo(this._datasource);

  @override
  Future<List<Product>> fetchAll({int page = 0}) {
    return _datasource.fetchAllProduct();
  }

  @override
  Future<List<Product>> fetchSearch(String search, {int page = 0}) async {
    return (await _datasource.fetchAllProduct())
        .where((element) => element.name.contains(search)).toList();
  }
}
