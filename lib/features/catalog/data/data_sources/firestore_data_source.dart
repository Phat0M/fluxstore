import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';

class FirestoreDataSource {
  final FirebaseFirestore firestore = FirebaseFirestore.instance;

  Future<List<Product>> fetchAllProduct() async {
    CollectionReference reference = firestore.collection('products');
    QuerySnapshot list = await reference.get();
    return list.docs.map((e) => Product.fromJson(e.data())).toList();
  }

  Future<void> updateUser(StoreUser user) async {
    CollectionReference reference = firestore.collection('users');
    final data = user.toJson();
    print(data);
    await reference
        .doc(user.id)
        .set(data)
        .catchError((e) => print('update user error: $e'));
  }
}
