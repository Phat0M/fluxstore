import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:fluxstore/common/util/exceptions.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';
import 'package:fluxstore/features/catalog/domain/use_cases/products_use_cases.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'catalog_bloc.freezed.dart';
part 'catalog_event.dart';
part 'catalog_state.dart';

class CatalogBloc extends Bloc<CatalogEvent, CatalogState> {
  CatalogBloc(this._fetchSearchUseCase, this._makeFavoriteUseCase)
      : super(InitialState());

  final FetchSearchUseCase _fetchSearchUseCase;
  final MakeFavoriteUseCase _makeFavoriteUseCase;

  String _search = '';
  List<Product> _products = [];

  Stream<CatalogState> _initialEvent(InitialEvent event) async* {
    yield LoadingState();
    try {
      _products = await _fetchSearchUseCase(SearchParams(_search));
      yield CatalogState.products(_products);
    } on UnauthorizedException catch (e, s) {
      yield UnauthError();
      rethrow;
    }
  }

  Stream<CatalogState> _searchEvent(SearchEvent event) async* {
    yield LoadingState();
    _search = event.search;
    try {
      _products = await _fetchSearchUseCase(SearchParams(_search));
      yield CatalogState.products(_products);
    } on UnauthorizedException catch (e, s) {
      yield UnauthError();
      rethrow;
    }
  }

  Stream<CatalogState> _updateEvent(UpdateEvent event) async* {
    yield LoadingState();
    try {
      _products = await _fetchSearchUseCase(SearchParams(_search));
      yield CatalogState.products(_products);
    } on UnauthorizedException catch (e, s) {
      yield UnauthError();
      rethrow;
    }
  }

  Stream<CatalogState> _selectEvent(SelectEvent event) async* {
    yield CatalogState.selected(
        _products.firstWhere((element) => element.id == event.key));
  }

  Stream<CatalogState> _makeFavoriteEvent(MakeFavoriteEvent event) async* {
    _makeFavoriteUseCase(FavoriteParams(event.key));
  }

  @override
  Stream<CatalogState> mapEventToState(
    CatalogEvent event,
  ) async* {
    yield* event.when(
      initial: () => _initialEvent(event),
      search: (_) => _searchEvent(event),
      update: () => _updateEvent(event),
      select: (_) => _selectEvent(event),
      makeFavorite: (String key) => _makeFavoriteEvent(event),
    );
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    super.onError(error, stackTrace);
    if (error is UnauthorizedException) {}
  }
}
