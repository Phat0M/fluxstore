// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'catalog_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CatalogEventTearOff {
  const _$CatalogEventTearOff();

// ignore: unused_element
  InitialEvent initial() {
    return const InitialEvent();
  }

// ignore: unused_element
  SearchEvent search(String search) {
    return SearchEvent(
      search,
    );
  }

// ignore: unused_element
  UpdateEvent update() {
    return const UpdateEvent();
  }

// ignore: unused_element
  SelectEvent select(String key) {
    return SelectEvent(
      key,
    );
  }

// ignore: unused_element
  MakeFavoriteEvent makeFavorite(String key) {
    return MakeFavoriteEvent(
      key,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CatalogEvent = _$CatalogEventTearOff();

/// @nodoc
mixin _$CatalogEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult search(String search),
    @required TResult update(),
    @required TResult select(String key),
    @required TResult makeFavorite(String key),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult search(String search),
    TResult update(),
    TResult select(String key),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialEvent value),
    @required TResult search(SearchEvent value),
    @required TResult update(UpdateEvent value),
    @required TResult select(SelectEvent value),
    @required TResult makeFavorite(MakeFavoriteEvent value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialEvent value),
    TResult search(SearchEvent value),
    TResult update(UpdateEvent value),
    TResult select(SelectEvent value),
    TResult makeFavorite(MakeFavoriteEvent value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $CatalogEventCopyWith<$Res> {
  factory $CatalogEventCopyWith(
          CatalogEvent value, $Res Function(CatalogEvent) then) =
      _$CatalogEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$CatalogEventCopyWithImpl<$Res> implements $CatalogEventCopyWith<$Res> {
  _$CatalogEventCopyWithImpl(this._value, this._then);

  final CatalogEvent _value;
  // ignore: unused_field
  final $Res Function(CatalogEvent) _then;
}

/// @nodoc
abstract class $InitialEventCopyWith<$Res> {
  factory $InitialEventCopyWith(
          InitialEvent value, $Res Function(InitialEvent) then) =
      _$InitialEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitialEventCopyWithImpl<$Res> extends _$CatalogEventCopyWithImpl<$Res>
    implements $InitialEventCopyWith<$Res> {
  _$InitialEventCopyWithImpl(
      InitialEvent _value, $Res Function(InitialEvent) _then)
      : super(_value, (v) => _then(v as InitialEvent));

  @override
  InitialEvent get _value => super._value as InitialEvent;
}

/// @nodoc
class _$InitialEvent implements InitialEvent {
  const _$InitialEvent();

  @override
  String toString() {
    return 'CatalogEvent.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is InitialEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult search(String search),
    @required TResult update(),
    @required TResult select(String key),
    @required TResult makeFavorite(String key),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult search(String search),
    TResult update(),
    TResult select(String key),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialEvent value),
    @required TResult search(SearchEvent value),
    @required TResult update(UpdateEvent value),
    @required TResult select(SelectEvent value),
    @required TResult makeFavorite(MakeFavoriteEvent value),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialEvent value),
    TResult search(SearchEvent value),
    TResult update(UpdateEvent value),
    TResult select(SelectEvent value),
    TResult makeFavorite(MakeFavoriteEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialEvent implements CatalogEvent {
  const factory InitialEvent() = _$InitialEvent;
}

/// @nodoc
abstract class $SearchEventCopyWith<$Res> {
  factory $SearchEventCopyWith(
          SearchEvent value, $Res Function(SearchEvent) then) =
      _$SearchEventCopyWithImpl<$Res>;
  $Res call({String search});
}

/// @nodoc
class _$SearchEventCopyWithImpl<$Res> extends _$CatalogEventCopyWithImpl<$Res>
    implements $SearchEventCopyWith<$Res> {
  _$SearchEventCopyWithImpl(
      SearchEvent _value, $Res Function(SearchEvent) _then)
      : super(_value, (v) => _then(v as SearchEvent));

  @override
  SearchEvent get _value => super._value as SearchEvent;

  @override
  $Res call({
    Object search = freezed,
  }) {
    return _then(SearchEvent(
      search == freezed ? _value.search : search as String,
    ));
  }
}

/// @nodoc
class _$SearchEvent implements SearchEvent {
  const _$SearchEvent(this.search) : assert(search != null);

  @override
  final String search;

  @override
  String toString() {
    return 'CatalogEvent.search(search: $search)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SearchEvent &&
            (identical(other.search, search) ||
                const DeepCollectionEquality().equals(other.search, search)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(search);

  @override
  $SearchEventCopyWith<SearchEvent> get copyWith =>
      _$SearchEventCopyWithImpl<SearchEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult search(String search),
    @required TResult update(),
    @required TResult select(String key),
    @required TResult makeFavorite(String key),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return search(this.search);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult search(String search),
    TResult update(),
    TResult select(String key),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (search != null) {
      return search(this.search);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialEvent value),
    @required TResult search(SearchEvent value),
    @required TResult update(UpdateEvent value),
    @required TResult select(SelectEvent value),
    @required TResult makeFavorite(MakeFavoriteEvent value),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return search(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialEvent value),
    TResult search(SearchEvent value),
    TResult update(UpdateEvent value),
    TResult select(SelectEvent value),
    TResult makeFavorite(MakeFavoriteEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (search != null) {
      return search(this);
    }
    return orElse();
  }
}

abstract class SearchEvent implements CatalogEvent {
  const factory SearchEvent(String search) = _$SearchEvent;

  String get search;
  $SearchEventCopyWith<SearchEvent> get copyWith;
}

/// @nodoc
abstract class $UpdateEventCopyWith<$Res> {
  factory $UpdateEventCopyWith(
          UpdateEvent value, $Res Function(UpdateEvent) then) =
      _$UpdateEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$UpdateEventCopyWithImpl<$Res> extends _$CatalogEventCopyWithImpl<$Res>
    implements $UpdateEventCopyWith<$Res> {
  _$UpdateEventCopyWithImpl(
      UpdateEvent _value, $Res Function(UpdateEvent) _then)
      : super(_value, (v) => _then(v as UpdateEvent));

  @override
  UpdateEvent get _value => super._value as UpdateEvent;
}

/// @nodoc
class _$UpdateEvent implements UpdateEvent {
  const _$UpdateEvent();

  @override
  String toString() {
    return 'CatalogEvent.update()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UpdateEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult search(String search),
    @required TResult update(),
    @required TResult select(String key),
    @required TResult makeFavorite(String key),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return update();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult search(String search),
    TResult update(),
    TResult select(String key),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (update != null) {
      return update();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialEvent value),
    @required TResult search(SearchEvent value),
    @required TResult update(UpdateEvent value),
    @required TResult select(SelectEvent value),
    @required TResult makeFavorite(MakeFavoriteEvent value),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return update(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialEvent value),
    TResult search(SearchEvent value),
    TResult update(UpdateEvent value),
    TResult select(SelectEvent value),
    TResult makeFavorite(MakeFavoriteEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (update != null) {
      return update(this);
    }
    return orElse();
  }
}

abstract class UpdateEvent implements CatalogEvent {
  const factory UpdateEvent() = _$UpdateEvent;
}

/// @nodoc
abstract class $SelectEventCopyWith<$Res> {
  factory $SelectEventCopyWith(
          SelectEvent value, $Res Function(SelectEvent) then) =
      _$SelectEventCopyWithImpl<$Res>;
  $Res call({String key});
}

/// @nodoc
class _$SelectEventCopyWithImpl<$Res> extends _$CatalogEventCopyWithImpl<$Res>
    implements $SelectEventCopyWith<$Res> {
  _$SelectEventCopyWithImpl(
      SelectEvent _value, $Res Function(SelectEvent) _then)
      : super(_value, (v) => _then(v as SelectEvent));

  @override
  SelectEvent get _value => super._value as SelectEvent;

  @override
  $Res call({
    Object key = freezed,
  }) {
    return _then(SelectEvent(
      key == freezed ? _value.key : key as String,
    ));
  }
}

/// @nodoc
class _$SelectEvent implements SelectEvent {
  const _$SelectEvent(this.key) : assert(key != null);

  @override
  final String key;

  @override
  String toString() {
    return 'CatalogEvent.select(key: $key)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SelectEvent &&
            (identical(other.key, key) ||
                const DeepCollectionEquality().equals(other.key, key)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(key);

  @override
  $SelectEventCopyWith<SelectEvent> get copyWith =>
      _$SelectEventCopyWithImpl<SelectEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult search(String search),
    @required TResult update(),
    @required TResult select(String key),
    @required TResult makeFavorite(String key),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return select(key);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult search(String search),
    TResult update(),
    TResult select(String key),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (select != null) {
      return select(key);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialEvent value),
    @required TResult search(SearchEvent value),
    @required TResult update(UpdateEvent value),
    @required TResult select(SelectEvent value),
    @required TResult makeFavorite(MakeFavoriteEvent value),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return select(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialEvent value),
    TResult search(SearchEvent value),
    TResult update(UpdateEvent value),
    TResult select(SelectEvent value),
    TResult makeFavorite(MakeFavoriteEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (select != null) {
      return select(this);
    }
    return orElse();
  }
}

abstract class SelectEvent implements CatalogEvent {
  const factory SelectEvent(String key) = _$SelectEvent;

  String get key;
  $SelectEventCopyWith<SelectEvent> get copyWith;
}

/// @nodoc
abstract class $MakeFavoriteEventCopyWith<$Res> {
  factory $MakeFavoriteEventCopyWith(
          MakeFavoriteEvent value, $Res Function(MakeFavoriteEvent) then) =
      _$MakeFavoriteEventCopyWithImpl<$Res>;
  $Res call({String key});
}

/// @nodoc
class _$MakeFavoriteEventCopyWithImpl<$Res>
    extends _$CatalogEventCopyWithImpl<$Res>
    implements $MakeFavoriteEventCopyWith<$Res> {
  _$MakeFavoriteEventCopyWithImpl(
      MakeFavoriteEvent _value, $Res Function(MakeFavoriteEvent) _then)
      : super(_value, (v) => _then(v as MakeFavoriteEvent));

  @override
  MakeFavoriteEvent get _value => super._value as MakeFavoriteEvent;

  @override
  $Res call({
    Object key = freezed,
  }) {
    return _then(MakeFavoriteEvent(
      key == freezed ? _value.key : key as String,
    ));
  }
}

/// @nodoc
class _$MakeFavoriteEvent implements MakeFavoriteEvent {
  const _$MakeFavoriteEvent(this.key) : assert(key != null);

  @override
  final String key;

  @override
  String toString() {
    return 'CatalogEvent.makeFavorite(key: $key)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is MakeFavoriteEvent &&
            (identical(other.key, key) ||
                const DeepCollectionEquality().equals(other.key, key)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(key);

  @override
  $MakeFavoriteEventCopyWith<MakeFavoriteEvent> get copyWith =>
      _$MakeFavoriteEventCopyWithImpl<MakeFavoriteEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult search(String search),
    @required TResult update(),
    @required TResult select(String key),
    @required TResult makeFavorite(String key),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return makeFavorite(key);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult search(String search),
    TResult update(),
    TResult select(String key),
    TResult makeFavorite(String key),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (makeFavorite != null) {
      return makeFavorite(key);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialEvent value),
    @required TResult search(SearchEvent value),
    @required TResult update(UpdateEvent value),
    @required TResult select(SelectEvent value),
    @required TResult makeFavorite(MakeFavoriteEvent value),
  }) {
    assert(initial != null);
    assert(search != null);
    assert(update != null);
    assert(select != null);
    assert(makeFavorite != null);
    return makeFavorite(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialEvent value),
    TResult search(SearchEvent value),
    TResult update(UpdateEvent value),
    TResult select(SelectEvent value),
    TResult makeFavorite(MakeFavoriteEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (makeFavorite != null) {
      return makeFavorite(this);
    }
    return orElse();
  }
}

abstract class MakeFavoriteEvent implements CatalogEvent {
  const factory MakeFavoriteEvent(String key) = _$MakeFavoriteEvent;

  String get key;
  $MakeFavoriteEventCopyWith<MakeFavoriteEvent> get copyWith;
}

/// @nodoc
class _$CatalogStateTearOff {
  const _$CatalogStateTearOff();

// ignore: unused_element
  InitialState initial() {
    return const InitialState();
  }

// ignore: unused_element
  LoadingState loading() {
    return const LoadingState();
  }

// ignore: unused_element
  ProductsState products(List<Product> products) {
    return ProductsState(
      products,
    );
  }

// ignore: unused_element
  UnauthError unauthorizedState() {
    return const UnauthError();
  }

// ignore: unused_element
  SelectedState selected(Product product) {
    return SelectedState(
      product,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CatalogState = _$CatalogStateTearOff();

/// @nodoc
mixin _$CatalogState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult products(List<Product> products),
    @required TResult unauthorizedState(),
    @required TResult selected(Product product),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult products(List<Product> products),
    TResult unauthorizedState(),
    TResult selected(Product product),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialState value),
    @required TResult loading(LoadingState value),
    @required TResult products(ProductsState value),
    @required TResult unauthorizedState(UnauthError value),
    @required TResult selected(SelectedState value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialState value),
    TResult loading(LoadingState value),
    TResult products(ProductsState value),
    TResult unauthorizedState(UnauthError value),
    TResult selected(SelectedState value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $CatalogStateCopyWith<$Res> {
  factory $CatalogStateCopyWith(
          CatalogState value, $Res Function(CatalogState) then) =
      _$CatalogStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$CatalogStateCopyWithImpl<$Res> implements $CatalogStateCopyWith<$Res> {
  _$CatalogStateCopyWithImpl(this._value, this._then);

  final CatalogState _value;
  // ignore: unused_field
  final $Res Function(CatalogState) _then;
}

/// @nodoc
abstract class $InitialStateCopyWith<$Res> {
  factory $InitialStateCopyWith(
          InitialState value, $Res Function(InitialState) then) =
      _$InitialStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitialStateCopyWithImpl<$Res> extends _$CatalogStateCopyWithImpl<$Res>
    implements $InitialStateCopyWith<$Res> {
  _$InitialStateCopyWithImpl(
      InitialState _value, $Res Function(InitialState) _then)
      : super(_value, (v) => _then(v as InitialState));

  @override
  InitialState get _value => super._value as InitialState;
}

/// @nodoc
class _$InitialState implements InitialState {
  const _$InitialState();

  @override
  String toString() {
    return 'CatalogState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is InitialState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult products(List<Product> products),
    @required TResult unauthorizedState(),
    @required TResult selected(Product product),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult products(List<Product> products),
    TResult unauthorizedState(),
    TResult selected(Product product),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialState value),
    @required TResult loading(LoadingState value),
    @required TResult products(ProductsState value),
    @required TResult unauthorizedState(UnauthError value),
    @required TResult selected(SelectedState value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialState value),
    TResult loading(LoadingState value),
    TResult products(ProductsState value),
    TResult unauthorizedState(UnauthError value),
    TResult selected(SelectedState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialState implements CatalogState {
  const factory InitialState() = _$InitialState;
}

/// @nodoc
abstract class $LoadingStateCopyWith<$Res> {
  factory $LoadingStateCopyWith(
          LoadingState value, $Res Function(LoadingState) then) =
      _$LoadingStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoadingStateCopyWithImpl<$Res> extends _$CatalogStateCopyWithImpl<$Res>
    implements $LoadingStateCopyWith<$Res> {
  _$LoadingStateCopyWithImpl(
      LoadingState _value, $Res Function(LoadingState) _then)
      : super(_value, (v) => _then(v as LoadingState));

  @override
  LoadingState get _value => super._value as LoadingState;
}

/// @nodoc
class _$LoadingState implements LoadingState {
  const _$LoadingState();

  @override
  String toString() {
    return 'CatalogState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoadingState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult products(List<Product> products),
    @required TResult unauthorizedState(),
    @required TResult selected(Product product),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult products(List<Product> products),
    TResult unauthorizedState(),
    TResult selected(Product product),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialState value),
    @required TResult loading(LoadingState value),
    @required TResult products(ProductsState value),
    @required TResult unauthorizedState(UnauthError value),
    @required TResult selected(SelectedState value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialState value),
    TResult loading(LoadingState value),
    TResult products(ProductsState value),
    TResult unauthorizedState(UnauthError value),
    TResult selected(SelectedState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingState implements CatalogState {
  const factory LoadingState() = _$LoadingState;
}

/// @nodoc
abstract class $ProductsStateCopyWith<$Res> {
  factory $ProductsStateCopyWith(
          ProductsState value, $Res Function(ProductsState) then) =
      _$ProductsStateCopyWithImpl<$Res>;
  $Res call({List<Product> products});
}

/// @nodoc
class _$ProductsStateCopyWithImpl<$Res> extends _$CatalogStateCopyWithImpl<$Res>
    implements $ProductsStateCopyWith<$Res> {
  _$ProductsStateCopyWithImpl(
      ProductsState _value, $Res Function(ProductsState) _then)
      : super(_value, (v) => _then(v as ProductsState));

  @override
  ProductsState get _value => super._value as ProductsState;

  @override
  $Res call({
    Object products = freezed,
  }) {
    return _then(ProductsState(
      products == freezed ? _value.products : products as List<Product>,
    ));
  }
}

/// @nodoc
class _$ProductsState implements ProductsState {
  const _$ProductsState(this.products) : assert(products != null);

  @override
  final List<Product> products;

  @override
  String toString() {
    return 'CatalogState.products(products: $products)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ProductsState &&
            (identical(other.products, products) ||
                const DeepCollectionEquality()
                    .equals(other.products, products)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(products);

  @override
  $ProductsStateCopyWith<ProductsState> get copyWith =>
      _$ProductsStateCopyWithImpl<ProductsState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult products(List<Product> products),
    @required TResult unauthorizedState(),
    @required TResult selected(Product product),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return products(this.products);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult products(List<Product> products),
    TResult unauthorizedState(),
    TResult selected(Product product),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (products != null) {
      return products(this.products);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialState value),
    @required TResult loading(LoadingState value),
    @required TResult products(ProductsState value),
    @required TResult unauthorizedState(UnauthError value),
    @required TResult selected(SelectedState value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return products(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialState value),
    TResult loading(LoadingState value),
    TResult products(ProductsState value),
    TResult unauthorizedState(UnauthError value),
    TResult selected(SelectedState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (products != null) {
      return products(this);
    }
    return orElse();
  }
}

abstract class ProductsState implements CatalogState {
  const factory ProductsState(List<Product> products) = _$ProductsState;

  List<Product> get products;
  $ProductsStateCopyWith<ProductsState> get copyWith;
}

/// @nodoc
abstract class $UnauthErrorCopyWith<$Res> {
  factory $UnauthErrorCopyWith(
          UnauthError value, $Res Function(UnauthError) then) =
      _$UnauthErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$UnauthErrorCopyWithImpl<$Res> extends _$CatalogStateCopyWithImpl<$Res>
    implements $UnauthErrorCopyWith<$Res> {
  _$UnauthErrorCopyWithImpl(
      UnauthError _value, $Res Function(UnauthError) _then)
      : super(_value, (v) => _then(v as UnauthError));

  @override
  UnauthError get _value => super._value as UnauthError;
}

/// @nodoc
class _$UnauthError implements UnauthError {
  const _$UnauthError();

  @override
  String toString() {
    return 'CatalogState.unauthorizedState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is UnauthError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult products(List<Product> products),
    @required TResult unauthorizedState(),
    @required TResult selected(Product product),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return unauthorizedState();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult products(List<Product> products),
    TResult unauthorizedState(),
    TResult selected(Product product),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unauthorizedState != null) {
      return unauthorizedState();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialState value),
    @required TResult loading(LoadingState value),
    @required TResult products(ProductsState value),
    @required TResult unauthorizedState(UnauthError value),
    @required TResult selected(SelectedState value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return unauthorizedState(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialState value),
    TResult loading(LoadingState value),
    TResult products(ProductsState value),
    TResult unauthorizedState(UnauthError value),
    TResult selected(SelectedState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (unauthorizedState != null) {
      return unauthorizedState(this);
    }
    return orElse();
  }
}

abstract class UnauthError implements CatalogState {
  const factory UnauthError() = _$UnauthError;
}

/// @nodoc
abstract class $SelectedStateCopyWith<$Res> {
  factory $SelectedStateCopyWith(
          SelectedState value, $Res Function(SelectedState) then) =
      _$SelectedStateCopyWithImpl<$Res>;
  $Res call({Product product});
}

/// @nodoc
class _$SelectedStateCopyWithImpl<$Res> extends _$CatalogStateCopyWithImpl<$Res>
    implements $SelectedStateCopyWith<$Res> {
  _$SelectedStateCopyWithImpl(
      SelectedState _value, $Res Function(SelectedState) _then)
      : super(_value, (v) => _then(v as SelectedState));

  @override
  SelectedState get _value => super._value as SelectedState;

  @override
  $Res call({
    Object product = freezed,
  }) {
    return _then(SelectedState(
      product == freezed ? _value.product : product as Product,
    ));
  }
}

/// @nodoc
class _$SelectedState implements SelectedState {
  const _$SelectedState(this.product) : assert(product != null);

  @override
  final Product product;

  @override
  String toString() {
    return 'CatalogState.selected(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SelectedState &&
            (identical(other.product, product) ||
                const DeepCollectionEquality().equals(other.product, product)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(product);

  @override
  $SelectedStateCopyWith<SelectedState> get copyWith =>
      _$SelectedStateCopyWithImpl<SelectedState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loading(),
    @required TResult products(List<Product> products),
    @required TResult unauthorizedState(),
    @required TResult selected(Product product),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return selected(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loading(),
    TResult products(List<Product> products),
    TResult unauthorizedState(),
    TResult selected(Product product),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (selected != null) {
      return selected(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialState value),
    @required TResult loading(LoadingState value),
    @required TResult products(ProductsState value),
    @required TResult unauthorizedState(UnauthError value),
    @required TResult selected(SelectedState value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(products != null);
    assert(unauthorizedState != null);
    assert(selected != null);
    return selected(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialState value),
    TResult loading(LoadingState value),
    TResult products(ProductsState value),
    TResult unauthorizedState(UnauthError value),
    TResult selected(SelectedState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (selected != null) {
      return selected(this);
    }
    return orElse();
  }
}

abstract class SelectedState implements CatalogState {
  const factory SelectedState(Product product) = _$SelectedState;

  Product get product;
  $SelectedStateCopyWith<SelectedState> get copyWith;
}
