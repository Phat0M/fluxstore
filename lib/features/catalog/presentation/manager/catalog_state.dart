part of 'catalog_bloc.dart';

@freezed
abstract class CatalogState with _$CatalogState {
  const factory CatalogState.initial() = InitialState;
  const factory CatalogState.loading() = LoadingState;
  const factory CatalogState.products(List<Product> products) = ProductsState;
  // реакция на попытку добавить в избранное не авторизированным бзером
  const factory CatalogState.unauthorizedState() = UnauthError;

  const factory CatalogState.selected(Product product) = SelectedState;
}
