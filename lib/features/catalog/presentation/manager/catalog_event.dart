part of 'catalog_bloc.dart';

@freezed
abstract class CatalogEvent with _$CatalogEvent {
  const factory CatalogEvent.initial() = InitialEvent;
  const factory CatalogEvent.search(String search) = SearchEvent;
  const factory CatalogEvent.update() = UpdateEvent;
  const factory CatalogEvent.select(String key) = SelectEvent;
  const factory CatalogEvent.makeFavorite(String key) = MakeFavoriteEvent;
}
