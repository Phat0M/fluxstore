import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';
import 'package:fluxstore/features/catalog/presentation/widgets/product_widget.dart';

typedef ItemCallBack<T> = void Function(T data);

class ProductsList extends StatelessWidget {
  final List<Product> products;
  final ItemCallBack<Product> onTap;
  final ItemCallBack<Product> onFavTap;
  final ItemCallBack<Product> onLongPress;

  const ProductsList({
    Key key,
    this.products,
    this.onTap,
    this.onFavTap,
    this.onLongPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      mainAxisSpacing: 10,
      crossAxisSpacing: 10,
      childAspectRatio: 1 / 2,
      shrinkWrap: true,
      children: List.generate(
        products.length,
            (index) => ProductWidget(
              product: products[index],
              onTap: () {
                onTap(products[index]);
              },
              onFavTap: () {
                onFavTap(products[index]);
              },
              onLongPress: () {
                onLongPress(products[index]);
              },
            ),
      ),
    );
  }
}
