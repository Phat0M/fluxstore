import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';

class ProductWidget extends StatefulWidget {
  final Product product;
  final VoidCallback onFavTap;
  final VoidCallback onTap;
  final VoidCallback onLongPress;

  const ProductWidget({
    Key key,
    this.product,
    this.onFavTap,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);

  @override
  _ProductWidgetState createState() => _ProductWidgetState();
}

class _ProductWidgetState extends State<ProductWidget> {

  bool isFav;

  @override
  void initState() {
    super.initState();
    isFav = widget.product.isFavorite;
  }

  @override
  void didUpdateWidget(covariant ProductWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    isFav = widget.product.isFavorite;
  }

  @override
  Widget build(BuildContext context) {
    print(widget.product);
    print(widget.product.rating / 5);
    return InkWell(
        onTap: widget.onTap,
        onLongPress: widget.onLongPress,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1 / 1.5,
                child: Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      child: Image.network(
                        widget.product.imageUrl,
                        fit: BoxFit.fill,
                        loadingBuilder:(BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null ?
                              loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                    if(widget.product.isFavorite != null) Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                          icon: Icon(isFav ? Icons.favorite : Icons.favorite_border),
                          onPressed: () {
                            setState(() {
                              isFav = !isFav;
                              widget.onFavTap();
                            });
                          }
                      ),
                    )
                  ],
                ),
              ),
              Text(
                widget.product.name ?? '',
                style: TextStyle(
                  color: const Color(0xff1d1f22),
                  fontSize: 14,
                ),
              ),
              SizedBox(height: 7,),
              Row(
                children: [
                  Text(
                    '\$ ${widget.product.discountPrice ?? widget.product.price}',
                    style: TextStyle(
                      color: const Color(0xff1d1f22),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  if(widget.product.discountPrice != null) Text(
                    '\$ ${widget.product.price}',
                    style: TextStyle(
                      color: const Color(0xffbebfc4),
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.lineThrough
                    ),
                  ),
                ],
              ),
              SizedBox(height: 7,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: 55,
                    child: ShaderMask(
                      shaderCallback: (bounds) => LinearGradient(
                        colors: [Color(0xff5ece7b), Colors.black],
                        stops: [widget.product.rating / 5, widget.product.rating / 5],
                      ).createShader(bounds),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(5, (index) => Icon(Icons.star, size: 11, color: Colors.white,),),
                      ),
                    ),
                  ),
                  Text(
                    '(${widget.product.ratingNumber})'
                  )
                ],
              ),
            ],
          ),
        ),
    );
  }
}

extension RandomColor on Colors {
  static Color random() {
    Random rnd = Random();
    return Color.fromRGBO(
        rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255), 1);
  }
}
