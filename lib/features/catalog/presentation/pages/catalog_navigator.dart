import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluxstore/features/catalog/presentation/pages/catalog_page.dart';
import 'package:fluxstore/features/catalog/presentation/pages/product_page.dart';

class CatalogNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: '/',
      onGenerateRoute: (settings) {
        Widget widget;
        switch(settings.name) {
          case '/':
            widget = CatalogPage();
            break;
          case '/product':
            final product = settings.arguments;
            widget = ProductPage(product: product,);
            break;
          default:
            widget = CatalogPage();
        }
        return MaterialPageRoute(builder: (_) => widget);
      },
    );
  }
}