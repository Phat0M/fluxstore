import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluxstore/features/catalog/presentation/manager/catalog_bloc.dart';
import 'package:fluxstore/features/catalog/presentation/widgets/products_list.dart';

class CatalogPage extends StatefulWidget {
  @override
  _CatalogPageState createState() => _CatalogPageState();
}

class _CatalogPageState extends State<CatalogPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _editingController;

  @override
  void initState() {
    super.initState();
    _editingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: theme.primaryColor,
      appBar: AppBar(
        elevation: 0,
        title: TextField(
          controller: _editingController,
          cursorColor: Colors.white,
          onChanged: (data) {
            context.read<CatalogBloc>().add(SearchEvent(data));
          },
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            filled: false,
            suffixIcon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            contentPadding:
                EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
          ),
        ),
        brightness: Brightness.dark,
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
          color: Colors.white,
        ),
        alignment: Alignment.center,
        child: BlocBuilder<CatalogBloc, CatalogState>(
          buildWhen: (old, now) {
            return now is ProductsState;
          },
          builder: (context, state) {
            return state.maybeWhen(
              products: (products) => ProductsList(
                products: products,
                onTap: (product) {
                  Navigator.of(context)
                      .pushNamed('/product', arguments: product);
                },
                onLongPress: (product) {
                  context.read<CatalogBloc>().add(SelectEvent(product.id));
                },
                onFavTap: (product) {
                  context
                      .read<CatalogBloc>()
                      .add(MakeFavoriteEvent(product.id));
                },
              ),
              loading: () => Center(
                child: CircularProgressIndicator(),
              ),
              orElse: () => Container(),
            );
          },
        ),
      ),
    );
  }
}
