import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluxstore/features/catalog/domain/entities/product.dart';

class ProductPage extends StatefulWidget {

  final Product product;

  const ProductPage({Key key, this.product}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {

  String currentSize = 'small';

  List<String> sizes = ['small', 'medium', 'big'];

  Color currentColor = Colors.red;

  Map<Color, String> colors = {
    Colors.red: 'red',
    Colors.blue: 'blue',
    Colors.green: 'green',
  };

  Widget propertyBuild<T>(String title, List<DropdownMenuItem> values, ValueChanged onChanged, T value) {

    return Container(
      padding: EdgeInsets.symmetric(vertical: 7, horizontal: 15),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xfff1f2f3), width: 1)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title, style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14),),
          DropdownButton(
          value: value,
          items: values,
          onChanged: onChanged,
        ),
      ]
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaquery = MediaQuery.of(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  width: mediaquery.size.width,
                    child: Image.network(widget.product.imageUrl, fit: BoxFit.fitWidth,)),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.all(10) + EdgeInsets.only(top: mediaquery.padding.top),
                    child: IconButton(
                      icon: Icon(Icons.clear, color: Colors.white,),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10,),
                  Text(
                    widget.product.name ?? '',
                    style: TextStyle(
                      color: const Color(0xff1d1f22),
                      fontSize: 14,
                    ),
                  ),
                  SizedBox(height: 7,),
                  Row(
                    children: [

                      if(widget.product.discountPrice != null) Text(
                        '\$ ${widget.product.price}',
                        style: TextStyle(
                            color: const Color(0xffbebfc4),
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            decoration: TextDecoration.lineThrough
                        ),
                      ),
                      SizedBox(width: 10,),
                      Text(
                        '\$ ${widget.product.discountPrice ?? widget.product.price}',
                        style: TextStyle(
                          color: const Color(0xff1d1f22),
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 7,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 55,
                        child: ShaderMask(
                          shaderCallback: (bounds) => LinearGradient(
                            colors: [Color(0xff5ece7b), Colors.black],
                            stops: [widget.product.rating / 5, widget.product.rating / 5],
                          ).createShader(bounds),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(5, (index) => Icon(Icons.star, size: 11, color: Colors.white,),),
                          ),
                        ),
                      ),
                      Text(
                          '(${widget.product.ratingNumber})'
                      )
                    ],
                  ),
                  propertyBuild('Size', List.generate(
                      sizes.length,
                          (index) => DropdownMenuItem(
                            value: sizes[index],
                              child: Text(
                                sizes[index],
                                style: TextStyle(fontSize: 14),),
                          )), (value) {
                        setState(() {
                          currentSize = value;
                        });
                    }, currentSize
                  ),
                  propertyBuild('Color', List.generate(
                      colors.length,
                          (i) => DropdownMenuItem(
                            value: colors.keys.toList()[i],
                              child: Row(
                                children: [
                                  Container(
                                    width: 30,
                                    height: 30,
                                    color: colors.keys.toList()[i],
                                  ),
                                  SizedBox(width: 5),
                                  Text(colors.values.toList()[i])
                                ],
                              ),)
                      ), (value) {
                    setState(() {
                      currentColor = value;
                    });
                  },
                    currentColor,
                  ),

                  RaisedButton(
                    color: Color(0xff5ece7b),
                      child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: Text('ADD TO CARD', style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14, color: Colors.white),)),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }
                  ),
                  Text(widget.product.description)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
