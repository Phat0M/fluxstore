import 'package:flutter/material.dart';
import 'package:fluxstore/features/auth/presentation/manager/auth_bloc.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:fluxstore/features/splash/presentation/widgets/preview_page_indicator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

const FIRST_RUN = "first run";

// функционала мало и расширятся вряд ли будем, поэтому блок создавать не будем
class PreviewScreen extends StatefulWidget {
  final Widget child;

  const PreviewScreen({Key key, this.child}) : super(key: key);

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen> {
  PageController _controller;
  bool isFirst;
  SharedPreferences sp;

  @override
  void initState() {
    super.initState();
    _controller = PageController(keepPage: true);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print(Provider.of<StoreUser>(context));
    sp = Provider.of<SharedPreferences>(context, listen: true);
    isFirst = sp.getBool(FIRST_RUN) ?? true;
  }

  @override
  Widget build(BuildContext context) {
    return isFirst
        ? Container(
            color: Colors.white,
            child: Column(
              children: [
                Expanded(
                  child: PageView(
                    controller: _controller,
                    children: [
                      _PreviewPage(
                        title: Text("Welcome to Fluxstore"),
                        imageAsset: 'assets/png/preview_1.png',
                        text: Text(
                            'Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit'),
                      ),
                      _PreviewPage(
                        title: Text("Lorconsectetur adipisicing"),
                        imageAsset: 'assets/png/preview_2.png',
                        text: Text(
                            'Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit'),
                      ),
                      _PreviewPage(
                        title: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              child: Text(
                                'Sign In',
                                style: TextStyle(
                                  color: Color(0xff5ece7b),
                                  fontSize: 22,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                              onTap: () {
                                context
                                    .read<AuthBloc>()
                                    ?.add(AuthEvent.google());
                              },
                            ),
                          ],
                        ),
                        imageAsset: 'assets/png/preview_3.png',
                        text: Text(
                            'Lorem Ipsum Dolor Sit Amet Consectetur Adipisicing Elit'),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 88,
                  child: PreviewPageIndicator(
                    controller: _controller,
                    backgroundColor: Colors.white,
                    count: 3,
                    onNextTap: () {
                      setState(() {
                        isFirst = false;
                        sp.setBool(FIRST_RUN, false);
                      });
                    },
                  ),
                ),
              ],
            ),
          )
        : widget.child;
  }
}

class _PreviewPage extends StatelessWidget {
  final Widget title;
  final String imageAsset;
  final Widget text;

  const _PreviewPage({
    Key key,
    @required this.title,
    @required this.imageAsset,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Material(
      child: Container(
        color: Colors.white,
        height: screenSize.height,
        width: screenSize.width,
        child: Column(
          children: [
            Expanded(
              child: Image.asset(imageAsset),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: 22,
                horizontal: 70,
              ),
              child: DefaultTextStyle(
                style: TextStyle(
                  fontSize: 22,
                  color: Colors.black,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
                child: title,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 13, horizontal: 70),
              child: DefaultTextStyle(
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xff676767),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
                child: text,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
