import 'package:flutter/material.dart';

class PreviewPageIndicator extends StatefulWidget {
  final PageController controller;
  final int count;
  final VoidCallback onNextTap;
  final Color backgroundColor;

  const PreviewPageIndicator({
    Key key,
    this.controller,
    this.count,
    this.onNextTap,
    this.backgroundColor,
  }) : super(key: key);

  @override
  _PreviewPageIndicatorState createState() => _PreviewPageIndicatorState();
}

class _PreviewPageIndicatorState extends State<PreviewPageIndicator> {
  PageController _controller;
  double position;
  int count;

  void _controllerListener() {
    setState(() {
      position = (_controller.page + 1) / count;
    });
  }

  @override
  void initState() {
    super.initState();
    _controller = widget.controller;
    count = widget.count;
    position = (_controller.initialPage + 1) / count;
    _controller.addListener(_controllerListener);
  }

  @override
  void didUpdateWidget(covariant PreviewPageIndicator oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (_controller != widget.controller) {
      _controller?.removeListener(_controllerListener);
      _controller = widget.controller;
      _controller.addListener(_controllerListener);
    }
    count = widget.count;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: widget.backgroundColor,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              width: 78,
              child: ShaderMask(
                shaderCallback: (bounds) => LinearGradient(
                  colors: [Colors.black, Color(0xffd6d6d6)],
                  stops: [position, position],
                ).createShader(bounds),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Indicator(),
                    SizedBox(width: 6),
                    Indicator(),
                    SizedBox(width: 6),
                    Indicator(),
                  ],
                ),
              ),
            ),
          ),
          if (widget.onNextTap != null)
            Visibility(
              visible: position.floor() == 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: Container(
                  padding: EdgeInsets.only(right: 30),
                  child: InkWell(
                    onTap: widget.onNextTap,
                    child: Text(
                      'Skip >',
                      style: TextStyle(color: Color(0xff5ec37b), fontSize: 16),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

class Indicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 2,
      width: 22,
      color: Color(0xffd6d6d6),
    );
  }
}
