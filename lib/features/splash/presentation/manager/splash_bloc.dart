import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluxstore/features/auth/data/data_sources/firestore_data_source.dart';
import 'package:fluxstore/features/auth/domain/use_cases/fetch_user_use_case.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'splash_bloc.freezed.dart';
part 'splash_event.dart';
part 'splash_state.dart';

// блок для первичной инициализации всего
class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc(this._fetchUser) : super(SplashState.initial());

  final FetchUserUseCase _fetchUser;

  Stream<SplashState> _onInit(InitSplashEvent event) async* {
    // Искусственно делаю задержку, в продакшене бы убрал
    await Future.delayed(Duration(seconds: 1));
    SharedPreferences sp = await SharedPreferences.getInstance();

    final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final currentUser = firebaseAuth.currentUser;
    StoreUser user;
    if(currentUser != null) {
        user = await _fetchUser(currentUser.email);
    }
    yield SplashState.create(
      sharedPref: sp,
      firebaseAuth: firebaseAuth,
      googleSignIn: googleSignIn,
      user: user,
    );
  }

  @override
  Stream<SplashState> mapEventToState(
    SplashEvent event,
  ) async* {
    yield* event.when(init: () => _onInit(event));
  }
}
