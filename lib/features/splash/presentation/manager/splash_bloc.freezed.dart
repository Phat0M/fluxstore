// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'splash_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SplashEventTearOff {
  const _$SplashEventTearOff();

// ignore: unused_element
  InitSplashEvent init() {
    return const InitSplashEvent();
  }
}

/// @nodoc
// ignore: unused_element
const $SplashEvent = _$SplashEventTearOff();

/// @nodoc
mixin _$SplashEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult init(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult init(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult init(InitSplashEvent value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult init(InitSplashEvent value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $SplashEventCopyWith<$Res> {
  factory $SplashEventCopyWith(
          SplashEvent value, $Res Function(SplashEvent) then) =
      _$SplashEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SplashEventCopyWithImpl<$Res> implements $SplashEventCopyWith<$Res> {
  _$SplashEventCopyWithImpl(this._value, this._then);

  final SplashEvent _value;
  // ignore: unused_field
  final $Res Function(SplashEvent) _then;
}

/// @nodoc
abstract class $InitSplashEventCopyWith<$Res> {
  factory $InitSplashEventCopyWith(
          InitSplashEvent value, $Res Function(InitSplashEvent) then) =
      _$InitSplashEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitSplashEventCopyWithImpl<$Res>
    extends _$SplashEventCopyWithImpl<$Res>
    implements $InitSplashEventCopyWith<$Res> {
  _$InitSplashEventCopyWithImpl(
      InitSplashEvent _value, $Res Function(InitSplashEvent) _then)
      : super(_value, (v) => _then(v as InitSplashEvent));

  @override
  InitSplashEvent get _value => super._value as InitSplashEvent;
}

/// @nodoc
class _$InitSplashEvent implements InitSplashEvent {
  const _$InitSplashEvent();

  @override
  String toString() {
    return 'SplashEvent.init()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is InitSplashEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult init(),
  }) {
    assert(init != null);
    return init();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult init(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult init(InitSplashEvent value),
  }) {
    assert(init != null);
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult init(InitSplashEvent value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitSplashEvent implements SplashEvent {
  const factory InitSplashEvent() = _$InitSplashEvent;
}

/// @nodoc
class _$SplashStateTearOff {
  const _$SplashStateTearOff();

// ignore: unused_element
  InitialSplashState initial() {
    return const InitialSplashState();
  }

// ignore: unused_element
  CreateSplashState create(
      {@required SharedPreferences sharedPref,
      @required FirebaseAuth firebaseAuth,
      @required GoogleSignIn googleSignIn,
      StoreUser user}) {
    return CreateSplashState(
      sharedPref: sharedPref,
      firebaseAuth: firebaseAuth,
      googleSignIn: googleSignIn,
      user: user,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SplashState = _$SplashStateTearOff();

/// @nodoc
mixin _$SplashState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult create(SharedPreferences sharedPref, FirebaseAuth firebaseAuth,
            GoogleSignIn googleSignIn, StoreUser user),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult create(SharedPreferences sharedPref, FirebaseAuth firebaseAuth,
        GoogleSignIn googleSignIn, StoreUser user),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialSplashState value),
    @required TResult create(CreateSplashState value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialSplashState value),
    TResult create(CreateSplashState value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $SplashStateCopyWith<$Res> {
  factory $SplashStateCopyWith(
          SplashState value, $Res Function(SplashState) then) =
      _$SplashStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SplashStateCopyWithImpl<$Res> implements $SplashStateCopyWith<$Res> {
  _$SplashStateCopyWithImpl(this._value, this._then);

  final SplashState _value;
  // ignore: unused_field
  final $Res Function(SplashState) _then;
}

/// @nodoc
abstract class $InitialSplashStateCopyWith<$Res> {
  factory $InitialSplashStateCopyWith(
          InitialSplashState value, $Res Function(InitialSplashState) then) =
      _$InitialSplashStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$InitialSplashStateCopyWithImpl<$Res>
    extends _$SplashStateCopyWithImpl<$Res>
    implements $InitialSplashStateCopyWith<$Res> {
  _$InitialSplashStateCopyWithImpl(
      InitialSplashState _value, $Res Function(InitialSplashState) _then)
      : super(_value, (v) => _then(v as InitialSplashState));

  @override
  InitialSplashState get _value => super._value as InitialSplashState;
}

/// @nodoc
class _$InitialSplashState implements InitialSplashState {
  const _$InitialSplashState();

  @override
  String toString() {
    return 'SplashState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is InitialSplashState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult create(SharedPreferences sharedPref, FirebaseAuth firebaseAuth,
            GoogleSignIn googleSignIn, StoreUser user),
  }) {
    assert(initial != null);
    assert(create != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult create(SharedPreferences sharedPref, FirebaseAuth firebaseAuth,
        GoogleSignIn googleSignIn, StoreUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialSplashState value),
    @required TResult create(CreateSplashState value),
  }) {
    assert(initial != null);
    assert(create != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialSplashState value),
    TResult create(CreateSplashState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialSplashState implements SplashState {
  const factory InitialSplashState() = _$InitialSplashState;
}

/// @nodoc
abstract class $CreateSplashStateCopyWith<$Res> {
  factory $CreateSplashStateCopyWith(
          CreateSplashState value, $Res Function(CreateSplashState) then) =
      _$CreateSplashStateCopyWithImpl<$Res>;
  $Res call(
      {SharedPreferences sharedPref,
      FirebaseAuth firebaseAuth,
      GoogleSignIn googleSignIn,
      StoreUser user});
}

/// @nodoc
class _$CreateSplashStateCopyWithImpl<$Res>
    extends _$SplashStateCopyWithImpl<$Res>
    implements $CreateSplashStateCopyWith<$Res> {
  _$CreateSplashStateCopyWithImpl(
      CreateSplashState _value, $Res Function(CreateSplashState) _then)
      : super(_value, (v) => _then(v as CreateSplashState));

  @override
  CreateSplashState get _value => super._value as CreateSplashState;

  @override
  $Res call({
    Object sharedPref = freezed,
    Object firebaseAuth = freezed,
    Object googleSignIn = freezed,
    Object user = freezed,
  }) {
    return _then(CreateSplashState(
      sharedPref: sharedPref == freezed
          ? _value.sharedPref
          : sharedPref as SharedPreferences,
      firebaseAuth: firebaseAuth == freezed
          ? _value.firebaseAuth
          : firebaseAuth as FirebaseAuth,
      googleSignIn: googleSignIn == freezed
          ? _value.googleSignIn
          : googleSignIn as GoogleSignIn,
      user: user == freezed ? _value.user : user as StoreUser,
    ));
  }
}

/// @nodoc
class _$CreateSplashState implements CreateSplashState {
  const _$CreateSplashState(
      {@required this.sharedPref,
      @required this.firebaseAuth,
      @required this.googleSignIn,
      this.user})
      : assert(sharedPref != null),
        assert(firebaseAuth != null),
        assert(googleSignIn != null);

  @override
  final SharedPreferences sharedPref;
  @override
  final FirebaseAuth firebaseAuth;
  @override
  final GoogleSignIn googleSignIn;
  @override
  final StoreUser user;

  @override
  String toString() {
    return 'SplashState.create(sharedPref: $sharedPref, firebaseAuth: $firebaseAuth, googleSignIn: $googleSignIn, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is CreateSplashState &&
            (identical(other.sharedPref, sharedPref) ||
                const DeepCollectionEquality()
                    .equals(other.sharedPref, sharedPref)) &&
            (identical(other.firebaseAuth, firebaseAuth) ||
                const DeepCollectionEquality()
                    .equals(other.firebaseAuth, firebaseAuth)) &&
            (identical(other.googleSignIn, googleSignIn) ||
                const DeepCollectionEquality()
                    .equals(other.googleSignIn, googleSignIn)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(sharedPref) ^
      const DeepCollectionEquality().hash(firebaseAuth) ^
      const DeepCollectionEquality().hash(googleSignIn) ^
      const DeepCollectionEquality().hash(user);

  @override
  $CreateSplashStateCopyWith<CreateSplashState> get copyWith =>
      _$CreateSplashStateCopyWithImpl<CreateSplashState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required
        TResult create(SharedPreferences sharedPref, FirebaseAuth firebaseAuth,
            GoogleSignIn googleSignIn, StoreUser user),
  }) {
    assert(initial != null);
    assert(create != null);
    return create(sharedPref, firebaseAuth, googleSignIn, user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult create(SharedPreferences sharedPref, FirebaseAuth firebaseAuth,
        GoogleSignIn googleSignIn, StoreUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (create != null) {
      return create(sharedPref, firebaseAuth, googleSignIn, user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(InitialSplashState value),
    @required TResult create(CreateSplashState value),
  }) {
    assert(initial != null);
    assert(create != null);
    return create(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(InitialSplashState value),
    TResult create(CreateSplashState value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (create != null) {
      return create(this);
    }
    return orElse();
  }
}

abstract class CreateSplashState implements SplashState {
  const factory CreateSplashState(
      {@required SharedPreferences sharedPref,
      @required FirebaseAuth firebaseAuth,
      @required GoogleSignIn googleSignIn,
      StoreUser user}) = _$CreateSplashState;

  SharedPreferences get sharedPref;
  FirebaseAuth get firebaseAuth;
  GoogleSignIn get googleSignIn;
  StoreUser get user;
  $CreateSplashStateCopyWith<CreateSplashState> get copyWith;
}
