part of 'splash_bloc.dart';

@freezed
abstract class SplashState with _$SplashState {
  const factory SplashState.initial() = InitialSplashState;
  const factory SplashState.create({
    @required SharedPreferences sharedPref,
    @required FirebaseAuth firebaseAuth,
    @required GoogleSignIn googleSignIn,
    StoreUser user,
  }) = CreateSplashState;
}
