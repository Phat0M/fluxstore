import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluxstore/features/auth/data/data_sources/firestore_data_source.dart';
import 'package:fluxstore/features/auth/data/repositories/firestore_store_user_repo.dart';
import 'package:fluxstore/features/auth/domain/use_cases/fetch_user_use_case.dart';
import 'package:fluxstore/features/auth/domain/use_cases/google_sign_in_use_case.dart';
import 'package:fluxstore/features/auth/presentation/manager/auth_bloc.dart';
import 'package:fluxstore/features/splash/presentation/manager/splash_bloc.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  final Widget child;

  const SplashScreen({Key key, this.child}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SplashBloc _bloc;
  FirestoreDataSource firestoreDataSource;

  @override
  void initState() {
    super.initState();
    firestoreDataSource = FirestoreDataSource();
    _bloc = SplashBloc(
        FetchUserUseCase(FirestoreStoreUserRepo(firestoreDataSource)))
      ..add(InitSplashEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SplashBloc, SplashState>(
      cubit: _bloc,
      builder: (context, state) {
        return state.maybeWhen(
          initial: () => _SplashWidget(),
          create: (sp, firebaseAuth, googleSignIn, user) {
            return MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) {
                    GoogleSignInUseCase googleSignInUseCase =
                        GoogleSignInUseCase(firebaseAuth, googleSignIn);
                    FetchUserUseCase fetchUser = FetchUserUseCase(
                        FirestoreStoreUserRepo(firestoreDataSource));
                    return AuthBloc(
                      user != null
                          ? AuthState.authorized(user)
                          : AuthState.unauthorized(),
                      googleSignInUseCase,
                      firebaseAuth,
                      fetchUser,
                    );
                  },
                ),
              ],
              child: MultiProvider(
                providers: [
                  Provider.value(
                    value: sp,
                  ),
                  Provider.value(
                    value: firestoreDataSource,
                  ),
                  Provider.value(
                    value: firebaseAuth,
                  ),
                ],
                child: BlocBuilder<AuthBloc, AuthState>(buildWhen: (old, now) {
                  return now is AuthorizedState || now is UnauthorizedState;
                }, builder: (context, state) {
                  print(state);
                  final user = state.maybeWhen(
                    authorized: (user) => user,
                    orElse: () => null,
                  );
                  print(user);
                  return Provider.value(
                    value: user,
                    child: widget.child,
                  );
                }),
              ),
            );
          },
          orElse: () => _SplashWidget(),
        );
      },
    );
  }
}

class _SplashWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Text(
          "Тут должен быть сплжш скрин :)",
          style: Theme.of(context).textTheme?.subtitle1,
          textDirection: TextDirection.ltr,
        ),
      ),
    );
  }
}
