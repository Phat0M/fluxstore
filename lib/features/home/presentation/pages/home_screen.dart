import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluxstore/features/auth/presentation/pages/auth_page.dart';
import 'package:fluxstore/features/catalog/data/data_sources/firestore_data_source.dart';
import 'package:fluxstore/features/catalog/data/repositories/firestore_products_repo.dart';
import 'package:fluxstore/features/catalog/data/repositories/firestore_user_repo.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:fluxstore/features/catalog/domain/use_cases/products_use_cases.dart';
import 'package:fluxstore/features/catalog/presentation/manager/catalog_bloc.dart';
import 'package:fluxstore/features/catalog/presentation/pages/catalog_navigator.dart';
import 'package:fluxstore/features/catalog/presentation/pages/catalog_page.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentPage = 0;

  StoreUser user;
  List<Widget> pages;

  CatalogBloc _catalogBloc;

  void onNavBarTap(int newPage) {
    setState(() {
      currentPage = newPage;
    });
  }

  @override
  void initState() {
    super.initState();
    user = Provider.of<StoreUser>(context, listen: false);
    FirestoreDataSource datasource = FirestoreDataSource();
    final productRepo = FirestoreProductsRepo(datasource);
    final userRepo = FirestoreUserRepo(datasource);
    print('home: $user');
    _catalogBloc = CatalogBloc(
      FetchSearchUseCase(productRepo, user),
      MakeFavoriteUseCase(userRepo, user),
    );
    _catalogBloc.add(InitialEvent());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    StoreUser newUser = Provider.of<StoreUser>(context, listen: true);
    if (user != newUser) {
      user = newUser;
      _catalogBloc?.close();
      FirestoreDataSource datasource = FirestoreDataSource();
      final productRepo = FirestoreProductsRepo(datasource);
      final userRepo = FirestoreUserRepo(datasource);
      print('home: $user');
      _catalogBloc = CatalogBloc(
        FetchSearchUseCase(productRepo, user),
        MakeFavoriteUseCase(userRepo, user),
      );
      _catalogBloc.add(InitialEvent());
    }
    pages = [
      Container(
        alignment: Alignment.center,
        child: Text('$currentPage'),
      ),
      BlocProvider.value(
        value: _catalogBloc,
        child: CatalogNavigator(),
      ),
      Container(
        alignment: Alignment.center,
        child: Text('$currentPage'),
      ),
      AuthScreen(
        child: Container(
          alignment: Alignment.center,
          child: Text('authorized'),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[currentPage],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentPage,
        onTap: onNavBarTap,
        items: [
          BottomNavigationBarItem(
            label: '',
            icon: Image.asset('assets/png/ic_home.png'),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Image.asset('assets/png/ic_catalog.png'),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Image.asset('assets/png/ic_basket.png'),
          ),
          BottomNavigationBarItem(
            label: '',
            icon: Image.asset('assets/png/ic_profile.png'),
          ),
        ],
      ),
    );
  }
}
