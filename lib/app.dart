import 'package:flutter/material.dart';
import 'package:fluxstore/features/catalog/domain/entities/store_user.dart';
import 'package:fluxstore/features/home/presentation/pages/home_screen.dart';
import 'package:fluxstore/features/preview/presentation/pages/preview_screen.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print(Provider.of<StoreUser>(context));
    return MaterialApp(
      title: 'Fluxstore',
      theme: ThemeData(
        primaryColor: Color(0xff5ECE7B),
        fontFamily: 'Raleway',
      ),
      home: PreviewScreen(
        child: HomeScreen(),
      ),
    );
  }
}
